import { EscalaMesPage } from './../pages/escala/escala-mes/escala-mes';
import { DialogService } from './service/dialog.service';
import { LoginPage } from './../pages/login/login';
import { AuthInterceptor } from './service/auth.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TranslateModule } from './util/translate/translate.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UtilService } from './service/util.service';
import { EscalaDiaPage } from '../pages/escala/escala-dia/escala-dia';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    EscalaMesPage,
    EscalaDiaPage
  ],
  imports: [
    BrowserModule,
    TranslateModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    EscalaMesPage,
    EscalaDiaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DialogService,
    UtilService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ]
})
export class AppModule {}
