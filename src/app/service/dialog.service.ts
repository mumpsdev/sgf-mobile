import { TranslateService } from './../util/translate/translate.service';
import { UtilService } from './util.service';
import { secretToken } from './../util/values';
import {
    LoadingController,
    AlertController,
    ToastController,
    Loading,
    Alert
} from "ionic-angular";
import {
    Injectable
} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
//Modals

@Injectable()
export class DialogService {
    constructor(
        private translateService: TranslateService,
        private utilService: UtilService,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private toastCtrl: ToastController
    ) {
    }

    private titleModal: string = this.translateService.getValue("MENSAGEM");
    private messageModal: string = this.translateService.getValue("AGUARDE");
    private labelCancel = this.translateService.getValue("CANCELAR")
    private labelOk = this.translateService.getValue("OK")
    private isLoading = false;

    public openLoading(message: string, duration: number): Loading{
        let loader: Loading;
        let options = {};
        this.isLoading = true;
        if(!message){
            message = this.messageModal + "...";
        }
        if(duration > 0){
            options["duration"] = duration;
        }
        options["content"] = message;
        loader = this.loadingCtrl.create(options);
        loader.present();
        return loader;
    }
    
    public closeLoading(loader){
        if(loader){
            loader.dismiss();
        }
    }

    public showAlert(title: string, message: string): Alert{
        let alert: Alert;
        let options = {};
        if(!title){
            title = this.titleModal;
        }
        options["title"] = title;
        options["subTitle"] = message;
        options["buttons"] = [this.labelOk];

        alert = this.alertCtrl.create(options);
        alert.present();
        return alert;
    }

    public showConfirm(title: string, message: string, callbackOk: any, callbackCancel): Alert{
        let alert: Alert;
        let options = {};
        if(!title){
            title = this.titleModal;
        }
        options["title"] = title;
        options["subTitle"] = message;
        options["buttons"] = [
            {
                text: this.labelCancel,
                role: this.labelCancel,
                handler: callbackCancel
            },
            {
                text: this.labelOk,
                role: this.labelOk,
                handler: callbackOk
            }
        ];
        alert = this.alertCtrl.create(options);
        alert.present();
        return alert;
    }

    public showErrorModal(error:any){
        sessionStorage.removeItem(secretToken.TOKEN);
        if(error instanceof HttpErrorResponse){
            let msg = this.utilService.getMsgStatusError(error.status);
            this.showAlert(this.titleModal, msg);
        }
    }

    public showToast(message: string, duration: number){
        let toast = this.toastCtrl.create({
            message: message,
            dismissOnPageChange: false,
            duration: duration
          });
          toast.present();
    }
}