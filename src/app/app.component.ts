import { LoginPage } from './../pages/login/login';
import { Component } from '@angular/core';
import { Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  acoes: any;
  menuLinks: Array<any> = [];
  userLogged: any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  setAcoes(acoes: any){
    this.acoes = acoes;
    this.setMenu();
  }

  setMenu(){
    this.menuLinks = [];
    if(this.acoes){
      let acoesCopy = this.acoes.map(x => Object.assign({}, x));
      acoesCopy.forEach( acao => {
        if(!acao.id_no && acao.isMenu){
          this.menuLinks.push(acao);
        }else if(acao.id_no && acao.isMenu){
          let acaoExistente = this.menuLinks.find((ac) => ac.id == acao.id_no);
          if(acaoExistente){
            if(!acaoExistente.subMenus){
              acaoExistente.subMenus = new Array<any>();
            }
            acaoExistente.subMenus.push(acao);
          }
        }
      })
    }
  }

  openPage(acao: any){
    console.log(acao);
  }

  public getAcao(url: string, item: string, metodo: string){
    let acao = null;
    if(this.acoes){
      this.acoes.forEach( a => {
        if((url && url == a.url) && (item && item == a.item) && (metodo && metodo == a.metodo)){
          acao = a;
        }
      });
    }
    return acao;
  }

  getUserLogged(){
   return this.userLogged;
  }

  setUserLogged(user: any){
    console.log("Setou user");
    this.userLogged = user;
  }

}

