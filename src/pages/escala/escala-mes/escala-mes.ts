import { TranslateService } from './../../../app/util/translate/translate.service';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { DialogService } from './../../../app/service/dialog.service';
import { HttpClient } from '@angular/common/http';
import { URLs, oMsg } from '../../../app/util/values';
import { UtilService } from './../../../app/service/util.service';
import { MyApp } from './../../../app/app.component';
import { EscalaDiaPage } from '../escala-dia/escala-dia';

@Component({
  selector: 'page-escala-mes',
  templateUrl: 'escala-mes.html',
})
export class EscalaMesPage implements OnInit{
  private mes = {id: 0, mes: 0, nome: "", ano: 0, status: 0, statusNome: "", filialId: 0, filialNome: "", semanas: [], folgas:[]};
  private titulo: string;
  private listaStatus = {"0": "", '1': "ABERTO", "2": "LIBERADO", "3": "FECHADO", "4":  "CANCELADO", "5":  "REMOVIDO"};
  public isAdmin: boolean;
  userLogged: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public translateService: TranslateService,
    public dialogService: DialogService,
    public http: HttpClient,
    public utilService: UtilService
  ) {
  }

 
  ngOnInit(){
    // this.isAdmin = this.app.userLogged &&  this.app.userLogged.isAdmin;
    let mes = this.navParams.get("mes");
    let user = this.navParams.get("user");
    if(user){
      this.userLogged = user;
      this.isAdmin = this.userLogged.isAdmin;
    }
    if(mes){
      this.mes.status = mes.status;
      this.titulo = mes.ano + " - " + mes.nome;
      this.configurarMes(mes);
    }
  }

  configurarMes(mes){
    this.mes.id = mes.id;
    this.mes.mes = mes.mes;
    this.mes.filialId = mes.filialId;
    this.mes.ano = mes.ano;
    this.mes.nome = this.translateService.getValue(mes.nome);
    this.mes.filialNome = mes.filialNome;
    this.mes.status = mes.status;
    this.mes.statusNome = mes == 0 ?  " (" + this.translateService.getValue(this.mes.status[mes.status]) + ")": "";
    if(mes.status && mes.status != "undefined"){
      this.carregarMes();
    }else{
        this.iniciarEscalaMes(mes);
    }
  }

  carregarMes(){
    let loader = this.dialogService.openLoading(null, 0);
    this.http.get(URLs.localhost + "/api/v1/escala/ano/" + this.mes.ano + "/mes/" + this.mes.mes + "/filial/" + this.mes.filialId).subscribe(data => {
      let obj = data[oMsg.OBJ];
      if(obj["mes"]){
        let mes = obj["mes"][0];
        this.mes.status = mes.status;
     
        // this.app.removeActionFixed(this.idBtnFecharEscala);
        // this.app.removeActionFixed(this.idBtnGerarEscala);
        // if(this.mes.status == 1 && this.isAdmin){
        //   this.setBtnGerarEscala();
        // }else if(this.mes.status == 2 && this.isAdmin){
        //   this.setBtnFecharEscala();
        // }
        //Inserindo Título
        // this.setTitulo();
        // let tooltipFolgas = this.translateService.getValue("FUNCIONARIO_FOLGAS");
        //Verificando as funcionalidades dependendo do status do mês.
        this.setScalas(mes);
      }
      this.dialogService.closeLoading(loader);
    }, error => {
      this.dialogService.closeLoading(loader);
      this.dialogService.showErrorModal(error);
    });
  }

  setScalas(mes: any){
    let loader = this.dialogService.openLoading(null, 0);
    let numDias = 1;
    if(mes["semana"]){
      this.mes.semanas = [];
      let semanas = mes["semana"];
      semanas.forEach((se, indexSe) => {
        let semana = {
          id: se.id,
          label: se.label,
          dias: []
        }
        if(se["dia"]){
          let dias = se["dia"];
          dias.forEach((diaSemana, indexDia) => {
            //Verificando se a semana começa depois do primeiro dia da semana.
            if(indexDia == 0 && diaSemana.numDiaSemana > 1 && dias.length < 7){
              let complete = 7 - dias.length;
              for(let i  = 0; i < complete; i++){
                semana.dias.push({numDia: null, trabalhando: [], folgando: [], isFeriado: true});
              }
            }
            let dia = {
              id: diaSemana["id"],
              numDia: this.utilService.getDayInMonth(mes.ano, diaSemana["numDiaAno"]), 
              numDiaSemana: diaSemana["numDiaSemana"],
              numDiaAno: diaSemana["numDiaAno"],
              semanaId: diaSemana["semanaId"],
              trabalhando: [],
              folgando: []
            }
            if(diaSemana["escala"]){
              let escalas = diaSemana["escala"];
              escalas.forEach((escala, index) =>{
                let colaborador = escala["colaborador"];
                colaborador["idEscala"] = escala.id;
                colaborador["idDia"] = diaSemana["id"];
                colaborador["tipoEscala"] = escala["tipoEscala"];
                if(escala.tipoEscala == 1){
                  dia.trabalhando.push(colaborador);
                }if(escala.tipoEscala == 2){
                  dia.folgando.push(colaborador);
                }
              });
            }
            semana.dias.push(dia);
            numDias++;
          });
        }
        if(se["reserva"]){
          let reserva = se["reserva"];
          if(reserva){
            reserva.forEach((colaborador, index) => {
              this.addColaboradorReserva(colaborador);
            });
          }
        }
        //Verificando se a semana termina antes do setimo dia da semana.
        if(semana.dias.length < 7){
          for(let i = semana.dias.length; i < 7; i++){
            semana.dias.push({numDia: null, trabalhando: [], folgando: [], isFeriado: true});
          }
        }
        this.mes.semanas.push(semana);
      })
      // if(this.mes.semanas.length < 1){
      //   this.semEscala = true;
      // }
    }
    this.dialogService.closeLoading(loader);
  }

  addColaboradorReserva(coloborador: any){
    let existente = this.mes.folgas.find((col) => col.id == coloborador.id);
    if(existente){
      existente.quantidade++;
    }else{
      coloborador["quantidade"] = 1;
      this.mes.folgas.push(coloborador);
    }
  }

  setCalcColaboradoresComFolgas(){
    let totalFolgas = 0;
    this.mes.folgas.forEach( colaborador => {
      totalFolgas += colaborador.quantidade;
    });
    if(totalFolgas == 0){
      // this.app.removeActionFixed(this.idBtnFolgas);
    }else{
      // this.app.setBadgesActionsFixed(this.idBtnFolgas, totalFolgas);
    }
  }

  iniciarEscalaMes(mes: any){
    let loader = this.dialogService.openLoading(null, 0);
    let dados = {};
    dados["filial"] = mes.filialId;
    dados["mes"] = mes.mes;
    dados["ano"] = mes.ano;
    this.http.post(URLs.localhost + URLs.INICIAR_ESCALA_MES, dados).subscribe(data => {
      this.dialogService.closeLoading(loader);
      let obj = data["obj"];
      if(obj){
        let mesAno = obj["mesAno"];
        if(mesAno && mesAno.id){
          this.mes.id = mesAno.id;
          this.mes.status = mesAno.status;
          let msg = this.translateService.getValue("DESEJA_ABRIR_MES", [this.mes.nome, this.mes.ano + ""]);
          this.dialogService.showConfirm(null, msg, () =>{
            this.gerarEscalaMes();
          }, () =>{
            this.carregarMes();
          });
        }
      }
    }, error =>{
      this.dialogService.showErrorModal(error);
    });
}

gerarEscalaMes(){
  let msgLoading = this.translateService.getValue("GERANDO_ESCALA_AGUARDE");
  let loader = this.dialogService.openLoading(msgLoading, 0);
  if(this.mes.id && this.mes.id > 0){
    let dados = {};
    dados["id"] = this.mes.id;
    this.http.post(URLs.localhost + URLs.GERAR_ESCALA_MES, dados).subscribe(data => {
      this.dialogService.closeLoading(loader);
      let msg = this.translateService.getValue("ESCALA_GERADA_SUCESSO", [this.mes.nome, this.mes.ano + ""]);
      this.dialogService.showToast(msg, 4000);
      this.carregarMes();
    }, error => {
      this.dialogService.closeLoading(loader);
      this.dialogService.showErrorModal(error);
    });
  }
}

fecharEscalaMes(){
  let loader = this.dialogService.openLoading(null, 0);
  if(this.mes.id && this.mes.id > 0){
    let dados = {};
    dados["id"] = this.mes.id;
    dados["novoStatus"] = 3
    this.http.put(URLs.localhost + URLs.ESCALA_MUDAR_STATUS, dados).subscribe(data => {
      this.dialogService.closeLoading(loader);
      this.carregarMes();
      let msg = this.translateService.getValue("ESCALA_FECHADA_SUCESSO", [this.mes.nome, this.mes.ano + ""]);
      this.dialogService.showToast(msg, 4000);
    }, error => {
      this.dialogService.closeLoading(loader);
      this.dialogService.showErrorModal(error);
    });
    }
  }

  backPage(){
    this.navCtrl.pop();
  }

  carregarDia(dia: any){
    this.navCtrl.push(EscalaDiaPage, {"dia": dia, "isAdmin": this.isAdmin, "status": this.mes.status});
  }
}
