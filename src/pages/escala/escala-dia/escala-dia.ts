import { UtilService } from './../../../app/service/util.service';
import { TranslateService } from './../../../app/util/translate/translate.service';
import { URLs } from './../../../app/util/values';
import { HttpClient } from '@angular/common/http';
import { DialogService } from './../../../app/service/dialog.service';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
@Component({
  selector: 'page-escala-dia',
  templateUrl: 'escala-dia.html',
})
export class EscalaDiaPage implements OnInit{
  private dia: any;
  private mostrarColaboradores: any;
  private isAdmin: false;
  private status: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private dialogService: DialogService,
    private http: HttpClient,
    private translateService: TranslateService,
    private utilService: UtilService
  ) {
  }

  ionViewDidLoad() {
  }


  ngOnInit(){
    this.mostrarColaboradores = "trabalhando";
    let dia = this.navParams.get("dia");
    let isAdmin = this.navParams.get("isAdmin");
    let status = this.navParams.get("status");
    if(status){
      this.status = status;
    }
    if(isAdmin){
      this.isAdmin = isAdmin;
    }
    if(dia){
      this.dia = dia;
      console.log(dia);
    }
  }

  mudaEscala(colaborador: any){
    console.log(colaborador);
    let novaEscala = colaborador.tipoEscala == 1 ? 2 : 1;
    let loader = this.dialogService.openLoading(null, 0);
    let dados = {id: colaborador.idEscala, colaboradorId: colaborador.id,
                 diaId: colaborador.idDia, tipoEscala: colaborador.tipoEscala,
                tipoEscalaNova: novaEscala};
    this.http.put(URLs.localhost + URLs.ESCALA_MUDAR_TIPO_TRABALHO_DIA_COLABORADOR, dados).subscribe(data => {
      this.dialogService.closeLoading(loader);
      this.trocarColaboradorLista(colaborador, novaEscala);
      let msg = this.translateService.getValue("ESCALA_COLABORADOR_ATUALIZADA", colaborador["nome"]);
      this.dialogService.showToast(msg, 3000);
    }, error => {
      this.dialogService.closeLoading(loader);
      this.dialogService.showErrorModal(error);
    });
  }

  trocarColaboradorLista(colaborador: any, novaEscala: number){
    if(novaEscala == 1){
      let index = this.dia.folgando.indexOf(colaborador);
      if(index > -1){
        this.dia.folgando.splice(index, 1);
      }
      colaborador.tipoEscala = novaEscala;
      this.dia.trabalhando.push(colaborador);
    }else if(novaEscala == 2){
      let index = this.dia.trabalhando.indexOf(colaborador);
      if(index > -1){
        this.dia.trabalhando.splice(index, 1);
      }
      colaborador.tipoEscala = novaEscala;
      this.dia.folgando.push(colaborador);
    }
  }
}
