import { DialogService } from './../../app/service/dialog.service';
import { LoginPage } from './../login/login';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { secretToken, URLs, oMsg } from '../../app/util/values';
import { HttpClient } from '@angular/common/http';
import { EscalaMesPage } from '../escala/escala-mes/escala-mes';
import { MyApp } from '../../app/app.component';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{

  listaFiliais = [
   
  ];
  listaAnos: any;
  meses: any;
  filialSelecionada;
  filialNome;
  anoSelecionado;
  nomesMes = [
    "JANEIRO", "FEVEREIRO", "MARCO", "ABRIL",
    "MAIO", "JUNHO", "JULHO", "AGOSTO",
    "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"
  ]
  permissaoMostrarMeses = true;
  isAdmin: boolean;
  inscricaoInit: Subscription;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dialogService: DialogService,
    public http: HttpClient,
    public app: MyApp
  ) {
  }
  
  ionViewDidLoad() {
    setTimeout(() => {
      this.inscricaoInit = this.navCtrl.viewDidEnter.subscribe(page =>{
        if(page.name == "HomePage"){
          this.onInit();
        }
      });
    }, 1000);
  }
  
  ngOnInit(){
    this.onInit();
  }

  onInit(){
    console.log("OnInit");
    if(this.verificarSeExisteToken()){
      if(this.app.userLogged){
        this.isAdmin = this.app.userLogged.isAdmin
      }
      let acaoMostrarMeses = this.app.getAcao(URLs.LIST_ESCALAS_ANO_FILIAL, "list", "GET");
      if(!acaoMostrarMeses){
        this.permissaoMostrarMeses = false;
      }
      this.listaFiliais.push({id: 1, fantasia: "Angelo Marques Yes LTDA", selected: true});
      if(this.listaFiliais && this.listaFiliais.length >= 1){
        this.filialSelecionada = this.listaFiliais[0].id;
        this.filialNome = this.listaFiliais[0].fantasia;
        this.buscarAnosFilial();
      }
    }
  }
  verificarSeExisteToken(): boolean{
    let userLogged = this.app.getUserLogged();
    let token = sessionStorage.getItem(secretToken.TOKEN);
    if(!token || token == "undefined" || !userLogged){
       sessionStorage.removeItem(secretToken.TOKEN);
       this.navCtrl.setRoot(LoginPage);
       return false;
    }else{
      return true;
    }
  }

  public logout(){
    this.navCtrl.setRoot(LoginPage);
  }

  buscarAnosFilial(){
    if(this.filialSelecionada){
      let loader = this.dialogService.openLoading(null, 0);
      this.listaAnos = [];
      this.http.get(URLs.localhost + "/api/v1/escala/filial/" + this.filialSelecionada).subscribe(data => {
        let obj = data[oMsg.OBJ];
        if(obj["anos"]){
          this.listaAnos = obj["anos"] || [];
          if(this.listaAnos.length >= 1){
            this.anoSelecionado =  this.listaAnos[(this.listaAnos.length-1)].ano;
            this.buscarMesAnoFilial();
          }
          this.meses = [];
        }
        this.dialogService.closeLoading(loader);
      }, error => {
        this.dialogService.showErrorModal(error);
        this.dialogService.closeLoading(loader);
     });
    }
  }

  buscarMesAnoFilial(){
    if(this.filialSelecionada && this.anoSelecionado){
      let loader = this.dialogService.openLoading(null, 0);
      this.meses = [];
      this.http.get(URLs.localhost + "/api/v1/escala/ano/" + this.anoSelecionado + "/filial/" + this.filialSelecionada).subscribe(data => {
        let obj = data[oMsg.OBJ];
        if(obj["mes"]){
          let meses = obj["mes"];
          this.carregarMeses(meses);
        }
        this.dialogService.closeLoading(loader);
      }, error => {
        this.dialogService.showErrorModal(error);
        this.dialogService.closeLoading(loader);
     });
    }
  }

  carregarMeses(meses: Array<any>){
    this.meses = [];
    let indexMes = 0;
    this.nomesMes.forEach((nomeMes, index) => {
      let idLoop = undefined, mesLoop = (index + 1), anoLoop = this.anoSelecionado, 
        statusLoop = undefined, filialIdLoop = this.filialSelecionada, numMes = (index + 1);
      if(indexMes < meses.length && meses[indexMes].mes == numMes){
        idLoop = meses[indexMes].id;
        mesLoop = meses[indexMes].mes;
        statusLoop = meses[indexMes].status;
        anoLoop = meses[indexMes].ano;
        filialIdLoop = meses[indexMes].filialId;
        indexMes++;
      }
      this.meses.push({
        id: idLoop,
        mes: numMes,
        ano: anoLoop,
        nome: nomeMes,
        status: statusLoop,
        filialId: filialIdLoop,
        filialNome: this.filialNome,
        acoes: [{imgUrl: "assets/images/link-escala.png", tooltip: "ESCALA", link: "/escala-mes"}]
      })
    });
  }

  onChangeFilial(){
    this.buscarAnosFilial();
  }

  onChangeAnoMeses(){
    this.buscarMesAnoFilial();
  }

  mostrarEscalaMes(mes: any){
    this.navCtrl.push(EscalaMesPage, {"mes": mes, "user": this.app.userLogged});
  }

  ngOnDestroy(){
    if(this.inscricaoInit){
      this.inscricaoInit.unsubscribe();
    }
  }

}
