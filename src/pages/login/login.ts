import { HomePage } from './../home/home';
import { secretToken } from './../../app/util/values';
import { TranslateService } from './../../app/util/translate/translate.service';
import { DialogService } from './../../app/service/dialog.service';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {
  HttpClient,
  HttpErrorResponse
} from "@angular/common/http";
import { URLs, oMsg } from '../../app/util/values';
import {MyApp} from "../../app/app.component";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit{
  public user = {login:"", password: ""};
  private longinOUSenhaInvalidos: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public dialogService: DialogService,
    public translateService: TranslateService,
    public app: MyApp
  ) {
  }

  ionViewDidLoad() {//Métdo que só carrega uma vez.
  }

  ngOnInit(){
    this.longinOUSenhaInvalidos = this.translateService.getValue("LOGIN_SENHA_INVALIDOS");
    this.setLogout();
  }

  setLogout(){
    this.user = {login:"", password: ""};
    sessionStorage.removeItem(secretToken.TOKEN);
    
  }

  login(){
    if(this.user.login && this.user.password){
      let loader = this.dialogService.openLoading(null, 0);
      this.http.post(URLs.localhost + URLs.AUTH_AUTHENTICATE, this.user).subscribe(data => {
          let obj = data[oMsg.OBJ];
          if(obj[oMsg.COLABORADOR]){
            this.app.setUserLogged(obj[oMsg.COLABORADOR]);
          }
          if(obj[oMsg.ACOES]){
            this.app.setAcoes(obj[oMsg.ACOES]);
          }
          if(obj[secretToken.TOKEN] && obj[secretToken.TOKEN] != "undefined"){
            sessionStorage.setItem(secretToken.TOKEN, obj[secretToken.TOKEN]);
            this.navCtrl.setRoot(HomePage);
          }
          this.dialogService.closeLoading(loader);
        }, error => {
          this.dialogService.closeLoading(loader);
          if(error instanceof HttpErrorResponse){
            if(error.status == 401){  
              console.log(error);
              this.dialogService.showAlert(null, this.longinOUSenhaInvalidos);
              console.log(this.longinOUSenhaInvalidos);
            }else{
              this.dialogService.showErrorModal(error);
            }
          }
      });
    }
  }
}
